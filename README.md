# How to install?
1. Clone the repo: `git clone git@gitlab.com:tkeskic/pinkman.git`
2. Build and run the application: `docker compose up --build`
