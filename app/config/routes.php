<?php

return [
    'statistics' => ['StatisticsController', ['GET' => 'readAction', 'POST' => 'createAction', 'DELETE' => 'deleteAction', 'PATCH' => 'updateAction']],
    'test' => ['TestController', ['GET' => 'readAction']],
    '/' => ['IndexController', ['indexAction']]
];
