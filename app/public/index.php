<?php

declare(strict_types=1);

use App\Router;

require __DIR__ . '/../src/bootstrap.php';

$router = new Router();
$router->resolve($_SERVER['REQUEST_URI']);
