<?php

declare(strict_types=1);

namespace App\Manager;

use App\Integration\Facebook\FacebookClient;
use App\Integration\Google\GoogleClient;
use App\Integration\Twitter\TwitterClient;
use App\Mapper\StatisticsMapper;
use App\Model\Statistics;
use App\Repository\StatisticsRepository;

class StatisticsManager
{
    private StatisticsRepository $statisticsRepository;
    private FacebookClient $facebookClient;
    private TwitterClient $twitterClient;
    private GoogleClient $googleClient;
    private StatisticsMapper $mapper;

    public function __construct()
    {
//        $this->statisticsRepository = new StatisticsRepository();
        $this->mapper = new StatisticsMapper();
        $this->facebookClient = new FacebookClient();
        $this->twitterClient = new TwitterClient();
        $this->googleClient = new GoogleClient();
    }

    private function getFacebookStatistics(): Statistics
    {
        $response = $this->facebookClient->getStatistics();
        $statisticsArray = json_decode($response->getContent(), true);
        return $this->mapper->mapArrayToObject($statisticsArray);
    }

    private function getTwitterStatistics(): Statistics
    {
        $response = $this->twitterClient->getWebsite();
        $statisticsArray = json_decode($response->getContent(), true);
        return $this->mapper->mapArrayToObject($statisticsArray);
    }

    private function getGoogleStatistics(): Statistics
    {
        $response = $this->googleClient->getWebsite();
        $statisticsArray = json_decode($response->getContent(), true);
        return $this->mapper->mapArrayToObject($statisticsArray);
    }

    public function getStatisticsData(): array
    {
        $googleStatistics = $this->getGoogleStatistics();
        $twitterStatistics = $this->getTwitterStatistics();
        $facebookStatistics = $this->getFacebookStatistics();

        return ['google' => $googleStatistics, 'twitter' => $twitterStatistics, 'facebook' => $facebookStatistics];
    }

    public function createStatistics(array $data): Statistics
    {
        $statistics = $this->mapper->mapArrayToObject($data);
//        $this->statisticsRepository->create($statistics);
        return $statistics;
    }

    public function updateStatistics(int $id, array $data): Statistics
    {
        $statistics = $this->mapper->mapArrayToObject($data);
//        $this->statisticsRepository->update($id, $statistics);
        return $statistics;
    }

    public function deleteStatistics(int $id): void
    {
//        $this->statisticsRepository->delete($id);
    }
}