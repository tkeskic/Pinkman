<?php

declare(strict_types=1);

namespace App\Http;

class AbstractResponse
{
    protected string $error;
    protected int $statusCode;
    protected string $message;

    public function __construct(string $error = "", int $statusCode = 200, string $message='')
    {
        $this->error = $error;
        $this->statusCode = $statusCode;
        $this->message = $message;
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function setError(string $error): void
    {
        $this->error = $error;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function __serialize(): array
    {
        return ['error' => $this->error, 'statusCode' => $this->statusCode, 'message' => $this->message];
    }
}