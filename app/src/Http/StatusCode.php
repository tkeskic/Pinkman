<?php

declare(strict_types=1);

namespace App\Http;

class StatusCode
{
    public const HTTP_OK = 200;
    public const HTTP_CREATED = 201;
    public const HTTP_NO_CONTENT = 204;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_GONE = 410;
}
