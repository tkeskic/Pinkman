<?php

declare(strict_types=1);

namespace App\Http;

class JsonResponse extends AbstractResponse
{
    private mixed $data;

    public function __construct(mixed $data = null, string $error = '', int $statusCode = 200, string $message = '')
    {
        $this->data = $data;
        parent::__construct($error, $statusCode, $message);
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setData(mixed $data): void
    {
        $this->data = $data;
    }

    public function __toString(): string
    {
        return json_encode(self::__serialize(), JSON_FORCE_OBJECT);
    }

    public function __serialize(): array
    {
        return ['response' => parent::__serialize(), 'data' => $this->data];
    }
}