<?php

declare(strict_types=1);

namespace App\Http\Client\Exception;

use App\Exception\ExceptionInterface;

interface TransportExceptionInterface extends ExceptionInterface
{

}