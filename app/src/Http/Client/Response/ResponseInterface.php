<?php

declare(strict_types=1);

namespace App\Http\Client\Response;

interface ResponseInterface
{
    public const MESSAGE_NOT_AVAILABLE = 'Message not available';

    public function getContent(): string;

    public function getHeaders(): array;

    public function getData(): array;

    public function getStatusCode(): int;

    public function getMessage(): string;
}