<?php

declare(strict_types=1);

namespace App\Http\Client\Request;

interface RequestInterface
{
    public function getMethod(): string;

    public function getPath(): string;

    public function getBody(): ?string;

    public function getOptions(): ?array;
}