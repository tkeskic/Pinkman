<?php

declare(strict_types=1);

namespace App\Http\Client;

use App\Http\Client\Exception\TransportExceptionInterface;
use App\Http\Client\Request\RequestInterface;
use App\Http\Client\Response\ResponseInterface;

interface ClientInterface
{
    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface;
}