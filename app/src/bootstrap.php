<?php

declare(strict_types=1);

namespace App;

use App\Controller\AbstractController;
use App\Http\JsonResponse;
use App\Http\StatusCode;

class Autoloader
{
    public function load($className)
    {
        $path = __DIR__ . '/' . str_replace('App\\', '', $className) . '.php';
        $filteredPath = str_replace('\\', '/', $path);

        require_once $filteredPath;
    }
}

class Router
{
    public function __construct()
    {
        spl_autoload_register([new Autoloader(), 'load']);
    }

    private function getRoutes(): array
    {
        return require_once __DIR__ . '/../config/routes.php';
    }

    private function parseUrl(string $url): string
    {
        $page = explode('/', $url)[1];
        return !empty($page) ? $page : '/';
    }

    private function getRequestMethod(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function resolve(string $uri): void
    {
        $page = $this->parseUrl($uri);
        print($this->loadController($this->getRoutes(), $page));
    }

    private function loadController(array $routes, string $page): JsonResponse
    {
        foreach ($routes as $key => $value) {
            if ($page === $key) {
                $requestMethod = $this->getRequestMethod();
                $controllerPath = 'App\\Controller\\' . $value[0];

                /** @var AbstractController $controller */
                $controller = new $controllerPath;

                if ($page === '/') {
                    $methodName = (string)$value[1][0];
                    return $controller->$methodName();
                }

                if (isset($value[1][$requestMethod])) {
                    $methodName = (string)$value[1][$requestMethod];
                    return $controller->$methodName();
                } else {
                    throw new \Exception('Invalid route specified for current configuration');
                }
            }
        }

        return new JsonResponse(null, '404', StatusCode::HTTP_NOT_FOUND, 'Default route not configured');
    }
}
