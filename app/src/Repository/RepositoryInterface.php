<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\AbstractModel;

interface RepositoryInterface
{
    public function find(int $id);

    public function findOneBy(array $criteria, array $orderBy = null);

    public function findAll();

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null);

    public function create(AbstractModel $model);

    public function delete(int $id);

    public function update(int $id, AbstractModel $model);
}