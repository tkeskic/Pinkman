<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\AbstractModel;
use App\Model\Statistics;

class StatisticsRepository extends BaseRepository implements RepositoryInterface
{
    public function __construct()
    {
        self::$instance = BaseRepository::getInstance();
    }

    public function create(Statistics|AbstractModel $model)
    {
        $sql = 'INSERT INTO statistics (name, viewCount) VALUES (?,?)';
        $stmt = self::$instance->prepare($sql);
        $stmt->execute([$model->getName(), $model->getViewCount()]);
    }

    public function delete(int $id): void
    {
        $sql = 'DELETE FROM statistics WHERE id=?';
        $stmt= self::$instance->prepare($sql);
        $stmt->execute([$id]);
    }

    public function update(int $id, Statistics|AbstractModel $model): void
    {
        $sql = 'UPDATE statistics SET name=?, viewCount=? WHERE id=?';
        $stmt = self::$instance->prepare($sql);
        $stmt->execute([]);
    }

    public function find(int $id)
    {
        $sql = 'SELECT * FROM statistics WHERE id=?';
        $stmt = self::$instance->prepare($sql);
        $stmt->execute([$id]);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        // TODO: Implement findOneBy() method.
    }

    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null)
    {
        // TODO: Implement findBy() method.
    }
}
