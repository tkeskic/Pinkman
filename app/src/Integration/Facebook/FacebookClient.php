<?php

declare(strict_types=1);

namespace App\Integration\Facebook;

use App\Http\Client\ClientInterface;
use App\Http\Client\Exception\TransportExceptionInterface;
use App\Http\Client\Request\RequestInterface;
use App\Http\Client\Response\ResponseInterface;
use App\Integration\Facebook\Request\FacebookRequest;
use App\Integration\Facebook\Response\FacebookResponse;

class FacebookClient implements ClientInterface
{
    public function getStatistics(): ResponseInterface
    {
        $request = new FacebookRequest('GET', '/api/visits');

        return $this->sendRequest($request);
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            $content = file_get_contents(
                __DIR__ . '/../../../public/data/Facebook/' . $request->getPath() . '/statistics.json'
            );
            $analyticsResponse = new FacebookResponse(200, $content, []);
        } catch (TransportExceptionInterface $e) {
            throw $e;
        }

        return $analyticsResponse;
    }
}