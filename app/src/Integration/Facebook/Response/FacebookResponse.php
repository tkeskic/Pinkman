<?php

declare(strict_types=1);

namespace App\Integration\Facebook\Response;

use App\Http\Client\Exception\ResponseException;
use App\Http\Client\Response\ResponseInterface;

class FacebookResponse implements ResponseInterface
{
    private int $statusCode = 0;
    private string $content = '';
    private array $headers = [];
    private array $data = [];
    private string $message = '';

    /**
     * @throws ResponseException
     */
    public function __construct(int $statusCode, string $content, array $headers)
    {
        try {
            $this->statusCode = $statusCode;
            $this->content = $content;
            $this->headers = $headers;

            if (!empty($this->content)) {
                $this->data = (array)json_decode($this->content, true, 512, JSON_THROW_ON_ERROR);
            }

            $this->message = $this->data['result']['message'] ?? $this->data['message'] ?? self::MESSAGE_NOT_AVAILABLE;
        } catch (\JsonException) {
        } catch (\Throwable $e) {
            throw new ResponseException($e->getMessage(), previous: $e);
        }
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}