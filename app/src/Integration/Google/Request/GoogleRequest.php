<?php

declare(strict_types=1);

namespace App\Integration\Google\Request;

use App\Http\Client\Request\RequestInterface;

class GoogleRequest implements RequestInterface
{
    private string $method;

    private string $path;

    private ?string $body;

    private ?array $options;

    public function __construct(string $method, string $path, ?string $body = null, ?array $options = null)
    {
        $this->method = $method;
        $this->path = $path;
        $this->body = $body;
        $this->options = $options;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }
}