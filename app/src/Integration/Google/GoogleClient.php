<?php

declare(strict_types=1);

namespace App\Integration\Google;

use App\Google\Request\FacebookRequest;
use App\Google\Response\FacebookResponse;
use App\Http\Client\ClientInterface;
use App\Http\Client\Exception\TransportExceptionInterface;
use App\Http\Client\Request\RequestInterface;
use App\Http\Client\Response\ResponseInterface;
use App\Integration\Google\Request\GoogleRequest;
use App\Integration\Google\Response\GoogleResponse;

class GoogleClient implements ClientInterface
{
    public function getWebsite(): ResponseInterface
    {
        $request = new GoogleRequest('GET', '/api/visits');

        return $this->sendRequest($request);
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            $content = file_get_contents(
                __DIR__ . '/../../../public/data/Google/' . $request->getPath() . '/statistics.json'
            );
            $analyticsResponse = new GoogleResponse(200, $content, []);
        } catch (TransportExceptionInterface $e) {
            throw $e;
        }

        return $analyticsResponse;
    }
}