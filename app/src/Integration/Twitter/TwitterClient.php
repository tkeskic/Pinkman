<?php

declare(strict_types=1);

namespace App\Integration\Twitter;

use App\Http\Client\ClientInterface;
use App\Http\Client\Exception\TransportExceptionInterface;
use App\Http\Client\Request\RequestInterface;
use App\Http\Client\Response\ResponseInterface;
use App\Integration\Twitter\Request\TwitterRequest;
use App\Integration\Twitter\Response\TwitterResponse;

class TwitterClient implements ClientInterface
{
    public function getWebsite(): ResponseInterface
    {
        $request = new TwitterRequest('GET', '/api/visits');

        return $this->sendRequest($request);
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            $content = file_get_contents(
                __DIR__ . '/../../../public/data/Twitter/' . $request->getPath() . '/statistics.json'
            );
            $analyticsResponse = new TwitterResponse(200, $content, []);
        } catch (TransportExceptionInterface $e) {
            throw $e;
        }

        return $analyticsResponse;
    }
}