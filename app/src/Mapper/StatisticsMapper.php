<?php

declare(strict_types=1);

namespace App\Mapper;

use App\Model\Statistics;

class StatisticsMapper
{
    public function mapObjectToArray(Statistics $website): array
    {
        return ['id' => $website->getId(), 'name' => $website->getName(), 'viewCount' => $website->getViewCount()];
    }

    public function mapArrayToObject(array $data): Statistics
    {
        $website = new Statistics();
        $website->setName($data['name']);
        $website->setViewCount($data['viewCount']);
        $website->setId($data['id']);
        return $website;
    }
}