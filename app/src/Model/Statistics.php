<?php

declare(strict_types=1);

namespace App\Model;

class Statistics extends AbstractModel implements \JsonSerializable
{
    private int $id;

    private int $viewCount;

    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getViewCount(): int
    {
        return $this->viewCount;
    }

    public function setViewCount(int $viewCount): void
    {
        $this->viewCount = $viewCount;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function jsonSerialize(): array
    {
        return ['id'=> $this->id, 'name' => $this->name, 'viewCount' => $this->viewCount];
    }
}