<?php

declare(strict_types=1);

namespace App\Controller;

abstract class AbstractController
{
    protected function getRequestData(): array
    {
        $requestBody = file_get_contents('php://input');
        return json_decode($requestBody,true) ?? [];
    }

    protected function getResourceId(): int
    {
        $url = $_SERVER['REQUEST_URI'];
        return (int)explode('/', $url)[2] ?? 0;
    }
}
