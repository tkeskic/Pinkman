<?php

declare(strict_types=1);

namespace App\Controller;

use App\Http\JsonResponse;
use App\Http\StatusCode;

class IndexController
{
    public function indexAction(): JsonResponse
    {
        return new JsonResponse(null, '', StatusCode::HTTP_OK, 'Index controller');
    }
}
