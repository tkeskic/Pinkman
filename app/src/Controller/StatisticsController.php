<?php

declare(strict_types=1);

namespace App\Controller;

use App\Http\JsonResponse;
use App\Http\StatusCode;
use App\Manager\StatisticsManager;

class StatisticsController extends AbstractController
{
    private StatisticsManager $statisticsManager;

    public function __construct()
    {
        $this->statisticsManager = new StatisticsManager();
    }

    public function readAction(): JsonResponse
    {
        $website = $this->statisticsManager->getStatisticsData();
        return new JsonResponse($website, '', StatusCode::HTTP_OK, 'read');
    }

    public function createAction(): JsonResponse
    {
        $data = $this->getRequestData();
        $website = $this->statisticsManager->createStatistics($data);
        return new JsonResponse($website, '', StatusCode::HTTP_CREATED, 'created');
    }

    public function updateAction(): JsonResponse
    {
        $id = $this->getResourceId();
        $data = $this->getRequestData();
        $website = $this->statisticsManager->updateStatistics($id, $data);
        return new JsonResponse($website, '', StatusCode::HTTP_OK, 'updated');
    }

    public function deleteAction(): JsonResponse
    {
        $id = $this->getResourceId();
        $this->statisticsManager->deleteStatistics($id);
        return new JsonResponse(null, '', StatusCode::HTTP_GONE, 'deleted');
    }


}
