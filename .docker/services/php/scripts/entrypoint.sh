#!/bin/bash -e
echo "===== Running entrypoint script ====="

# Define paths
PROJECT_ROOT=$(realpath ..)
APP_DIR=$PROJECT_ROOT/app
BIN_DIR=$PROJECT_ROOT/bin/php
DOCKER_SCRIPTS_DIR=$PROJECT_ROOT/docker/services/php/scripts/
WAITERS_DIR=$DOCKER_SCRIPTS_DIR/waiters

# Switch to project directory for easier handling
cd $PROJECT_ROOT
echo "INFO: Current working directory: $PROJECT_ROOT"

# Wait for specific services to be up and running
MARIADB_HOST=mariadb-pinkman
MARIADB_PORT=3306
MARIADB_TIMEOUT=30

echo "INFO: Waiting for MariaDB.."
wait-for-it --host=$MARIADB_HOST --port=$MARIADB_PORT --timeout=$MARIADB_TIMEOUT

# Run post init commands after services are available
php -v

# Return to application directory
cd $APP_DIR
echo "INFO: Current working directory: $APP_DIR"

echo "===== Finished entrypoint script ====="

# Delegate current command
exec "$@"
